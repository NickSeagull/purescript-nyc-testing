# Integrating [nyc](https://github.com/istanbuljs/nyc) with PureScript

This is a simple demo that is being used in issue [#1081](https://github.com/istanbuljs/nyc/issues/1081) of the `nyc` repo
to try to integrate it with PureScript to allow code coverage.

## How to build

Install `spago` and `purescript`:

```bash
$ yarn global add spago purescript
# or alternatively
$ npm i -g spago purescript
```

To run tests with sourcemap support for nyc:

```bash
$ nyc spago test -- -g sourcemaps
```

## Where are the compiled sources located?

You can find the compiled sources and sourcemaps in:

```text
output/Foo
output/Test.Main
```

Note that this will be available only after the first test run, or after `spago build`

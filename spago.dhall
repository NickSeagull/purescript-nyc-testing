{ name =
    "purescript-nyc-testing"
, dependencies =
    [ "console", "effect", "spec" ]
, packages =
    ./packages.dhall
}

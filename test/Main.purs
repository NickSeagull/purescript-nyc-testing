module Test.Main where

import Prelude

import Effect (Effect)
import Test.Spec (describe, it)
import Test.Spec.Assertions (shouldEqual)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (run)

import Foo (foo)

main :: Effect Unit
main = run [consoleReporter] do
  describe "something" do
    it "tests" do
      foo `shouldEqual` 42